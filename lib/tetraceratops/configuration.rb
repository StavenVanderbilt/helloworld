# For reading configuration from config/configuration.yml.example
require 'syck'

module Tetraceratops
  module Configuration

    @global_config = nil
    @config        = nil


    class << self

      # Load yml config under config folder of from options
      def load_config
        global_config_path = File.join(Rails.root, 'config', 'configuration.yml')
        @global_config = read_config(global_config_path)
      end

      # Returns a configuration setting
      def [](key)
        load_config unless @global_config
        @global_config[key]
      end


      def read_config path
        if File.exist?(path)
          # initialize @config
          @config ||= {}

          begin
            file_name = File.basename(path, '.yml')
            @config[file_name] = Syck.load_file(path)
          rescue ArgumentError
            abort "Your configuration file located at #{path} is not a valid YAML file and could not be loaded."
          rescue Psych::SyntaxError => e
            abort "A syntax error occurred when parsing your configuration file located at #{path}\n#{e.backtrace.join("\n")}"
          end
        else
          raise "Configuration file '#{path}' not found"
        end
      end

    end

  end
end