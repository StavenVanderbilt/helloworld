class ChangeIndexForWelcomes < ActiveRecord::Migration[5.0]
  def change
    remove_column :welcomes, :index
    add_column :welcomes, :name, :string, null: false, limit: 20
    add_column :welcomes, :age, :integer, null: false
  end
end
