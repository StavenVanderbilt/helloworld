# production server machine
server '192.168.1.107', port: 22, user: "deploy",  roles: %w{app db web}
set :repo_url,        'git@192.168.1.118:stavenvanderbilt/helloworld.git' # local GitLab server Machine
set :user,            "deploy"
set :stage,           "local_production"
set :ssh_options, { :forward_agent => true }
# set :ssh_options,     { forward_agent: true, user: fetch(:user), keys: %w(~/.ssh/id_rsa.pub) }
# Default deploy_to directory is /var/www/my_app_name
set :deploy_to,       "/home/#{fetch(:user)}/apps/#{fetch(:application)}"

set :puma_threads, [0, 8]
set :puma_workers, 4
set :puma_env, "#{fetch(:stage)}"