# # config valid only for current version of Capistrano
lock "3.8.1"

# set :application, "my_app_name"
# set :repo_url, "git@example.com:me/my_repo.git"
#
# # Default branch is :master
# # ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
#
# # Default deploy_to directory is /var/www/my_app_name
# # set :deploy_to, "/var/www/my_app_name"
#
# # Default value for :format is :airbrussh.
# # set :format, :airbrussh
#
# # You can configure the Airbrussh format using :format_options.
# # These are the defaults.
# # set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto
#
# # Default value for :pty is false
# # set :pty, true
#
# # Default value for :linked_files is []
# # append :linked_files, "config/database.yml", "config/secrets.yml"
#
# # Default value for linked_dirs is []
# # append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"
#
# # Default value for default_env is {}
# # set :default_env, { path: "/opt/ruby/bin:$PATH" }
#
# # Default value for keep_releases is 5
# set :keep_releases, 5


# # Don't change these unless you know what you're doing
# set :pty,                 true
# set :use_sudo,            false
# set :deploy_via,          :remote_cache
# set :stages,              ["staging", "production"]
# set :default_stage,       "staging"
# set :deploy_to,           -> { "/home/#{fetch(:user)}/apps/#{fetch(:application)}" }
# set :puma_bind,           -> { "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock" }
# set :puma_state,          -> { "#{shared_path}/tmp/pids/puma.state" }
# set :puma_pid,            -> { "#{shared_path}/tmp/pids/puma.pid" }
# set :puma_access_log,     -> { "#{release_path}/log/puma.error.log" }
# set :puma_error_log,      -> { "#{release_path}/log/puma.access.log" }
# set :ssh_options,     { forward_agent: true, user: fetch(:user), keys: %w(~/.ssh/id_rsa.pub) }
# set :puma_preload_app, true
# set :puma_worker_timeout, nil
# set :puma_init_active_record, true  # Change to false when not using ActiveRecord

puts "==================================================================================================="
puts "===============================           deploy.rb          ======================================"
puts "==================================================================================================="

set :application,     'helloworld'
set :repo_url,        'git@bitbucket.org:StavenVanderbilt/helloworld.git'

set :stages,              ["staging", "production"]
set :default_stage,       "staging"

# Default value for :linked_files is []
append :linked_files, 'config/database.yml', 'config/puma.rb', 'config/configuration.yml', 'config/secrets.yml'

# Default value for linked_dirs is []
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle'

# use rvm gemset rather than bundle path for working with launchctl
set :rvm_type, :user
set :rvm_ruby_version, '2.3.1@helloworld'
set :bundle_path, nil
set :bundle_binstubs, nil
set :bundle_flags, '--system'

## Defaults:
# set :scm,           :git
# set :branch,        :master
set :format,        :pretty
set :log_level,     :debug
set :keep_releases, 5


set :pty,                 true
set :use_sudo,            false
set :puma_user,           fetch(:user)
set :puma_state,          -> { "#{shared_path}/tmp/pids/puma.state" }
set :puma_pid,            -> { "#{shared_path}/tmp/pids/puma.pid" }
set :puma_bind,           -> { "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock" }
set :puma_conf,           -> { "#{shared_path}/config/puma.rb" }
set :puma_access_log,     -> { "#{release_path}/log/puma.access.log" }
set :puma_error_log,      -> { "#{release_path}/log/puma.error.log" }
set :puma_role,           :app
set :puma_init_active_record, false # Change to false when not using ActiveRecord
set :puma_preload_app, true


def branch_name(default_branch)
  branch = ENV.fetch('BRANCH', default_branch)

  if branch == '.'
    # current branch
    `git rev-parse --abbrev-ref HEAD`.chomp
  else
    branch
  end
end

set :branch, branch_name('master')

namespace :nginx do
  desc 'setup config for nginx server ( how to use: cap stage_environment nginx:set_config )'
  task :set_config do
    on roles(:web) do
      execute :sudo, "rm -f /etc/nginx/sites-enabled/hellworld.conf"
      execute :sudo, "ln -s #{fetch(:deploy_to)}/current/config/deploy/nginx/nginx.conf /etc/nginx/sites-enabled/helloworld.conf"
      execute :sudo, "/usr/sbin/nginx -s quit || true"
      execute :sudo, "/usr/sbin/nginx"
    end
  end

  desc 'Reload nginx ( how to use: cap stage_environment nginx:reload )'
  task :reload do
    on roles(:web) do
      execute :sudo, "service nginx reload"
    end
  end

end


namespace :deploy do
  # after :finished, 'deploy:generate_build_number'
  # desc 'Generate build number'
  # task :generate_build_number do
  #   on roles(:app) do
  #     within current_path do
  #       execute "echo #{ENV.fetch('BUILD_NUMBER')} > #{current_path}/BUILD_NUMBER"
  #     end
  #   end
  # end

end

puts "==================================================================================================="

  namespace :puma do

    desc 'Kill puma service'
    task :kill_puma do
      on roles(:app) do
        within current_path do
          execute "ps -ef | grep puma | awk '{print $2}'| xargs kill -9"
        end
      end
    end

    desc 'Restart puma server again'
    task :restart_puma do
      on roles(:app) do
        within current_path do
          #invoke "deploy:kill_puma"
          # execute "bundle exec puma -e production -b unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock --pidfile #{shared_path}/tmp/pids/puma.pid &"
          # execute "bundle exec puma -C /home/deploy/apps/helloworld/shared/config/puma.rb --daemon"
          execute "bundle exec pumactl -S #{shared_path}/tmp/pids/puma.state -F #{shared_path}/config/puma.rb restart"
        end
      end

      puts "==========================================================="
      puts "puma_env:  #{puma_env}"
      puts "==========================================================="
    end

  end

# namespace :puma do
#   desc 'Create Directories for Puma Pids and Socket'
#   task :make_dirs do
#     on roles(:app) do
#       puts "=================  make_dirs  =================="
#       execute "mkdir #{shared_path}/tmp/sockets -p"
#       execute "mkdir #{shared_path}/tmp/pids -p"
#       puts "================================================"
#     end
#   end
#
#   before :start, :make_dirs
# end
#
# namespace :deploy do
#   desc "Make sure local git is in sync with remote."
#   task :check_revision do
#     puts "=================  check_revision  =================="
#     on roles(:app) do
#       unless `git rev-parse HEAD` == `git rev-parse origin/master`
#         puts "WARNING: HEAD is not the same as origin/master"
#         puts "Run `git push` to sync changes."
#         exit
#       end
#     end
#     Rake::Task["puma:start"].reenable
#     puts "====================================================="
#   end
#
#   desc 'Initial Deploy'
#   task :initial do
#     on roles(:app) do
#       puts  "==================   initial   =================="
#       before 'deploy:restart', 'puma:start'
#       invoke 'deploy'
#       puts "=================================================="
#     end
#   end
#
#   desc 'Restart application'
#   task :restart do
#     on roles(:app), in: :sequence, wait: 5 do
#       # invoke 'puma:restart'
#       puts "================   restart   =================="
#       Rake::Task["puma:restart"].reenable
#       puts "==============================================="
#     end
#   end
#
#   before :starting,     :check_revision
#   after  :finishing,    :compile_assets
#   after  :finishing,    :cleanup
#   after  :finishing,    :restart
# end

# ps aux | grep puma    # Get puma pid
# kill -s SIGUSR2 pid   # Restart puma
# kill -s SIGTERM pid   # Stop puma