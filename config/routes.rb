Rails.application.routes.draw do

  resources :welcomes

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'welcomes#index'

  get 'home/console'

  resources :articles do
    resources :comments
  end

end